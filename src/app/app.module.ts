import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonComponent } from './common/common.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { HomeComponent } from './featured/home/home.component';
import { PagenotfoundComponent } from './common/pagenotfound/pagenotfound.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { PortfolioComponent } from './featured/portfolio/portfolio.component';
import { AboutUsComponent } from './featured/aboutUs/about-us/about-us.component';
import { CareerComponent } from './featured/aboutUs/career/career.component';
import { TestimonialsComponent } from './featured/aboutUs/testimonials/testimonials.component';
import { ContactModule } from './featured/contact/contact.module';

import { ErrorInterceptor } from './shared/interceptor';
import { ScrollToTopComponent } from './common/scroll-to-top/scroll-to-top.component';
import { SpinnerComponent } from './common/spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    CommonComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    PagenotfoundComponent,
    PortfolioComponent,
    AboutUsComponent,
    CareerComponent,
    TestimonialsComponent,
    ScrollToTopComponent,
    SpinnerComponent,

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule, HttpClientModule, BrowserAnimationsModule, ReactiveFormsModule, NgxPaginationModule, ContactModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
