import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../shared/api.service';
import { Meta, Title } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})
export class TestimonialsComponent implements OnInit {
  slug: any;
  data: any;
  testimonialData: any;
  constructor(private route: ActivatedRoute, private apiservice: ApiService, private title: Title, private meta: Meta) {

    console.log(route?.routeConfig?.path)
  }

  ngOnInit(): void {
    this.getTestimonial();
  }
  getTestimonial() {
    this.apiservice.getTestimonialData().subscribe((res) => {
      this.data = <any>res;
      this.testimonialData = this.data;

      setTimeout(function () {
        $(".owl-carousel").owlCarousel({
          items: 1,
          loop: true,
          nav: false,
          dots: false,
          autoplay: true,
          autoplaySpeed: 100,
          smartSpeed: 150,
          autoplayHoverPause: true,
        });
      }, 0);
      this.title.setTitle('Testimonial');
      this.meta.addTag({ name: "description", content: this.testimonialData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.testimonialData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.testimonialData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.testimonialData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/testimonials/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
    },
      (error) => {
        console.log(error)
      })
  }
}
