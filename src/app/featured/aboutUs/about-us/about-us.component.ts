import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/shared/api.service';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  slug: any;
  data: any;
  aboutusData: any;
  constructor(private apiservice: ApiService, private title: Title, private meta: Meta) {

  }

  ngOnInit(): void {
    this.getAboutusContent();
  }

  getAboutusContent() {
    this.apiservice.getAbout().subscribe((res) => {
      this.data = <any>res;
      this.aboutusData = this.data;
      this.title.setTitle(this.aboutusData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.aboutusData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.aboutusData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.aboutusData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: "Our Company Vision We will increase the value of our company and our global portfolio of diversified brands by exceeding customers&#8217; expectations and achieving market leadership and operating excellence in every segment of our company. Since 2007 Ut sit amet dignissim libero. Integer scelerisque, eros interdum suscipit rhoncus, mauris felis eleifend libero, a adipiscing arcu &hellip; Continue reading &quot;About Us&quot;" });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/about-us/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
    },
      (error) => {
        console.log(error)
      })
  }

}
