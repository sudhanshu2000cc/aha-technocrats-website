import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ApiService } from '../../../shared/api.service';

@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.css']
})
export class CareerComponent implements OnInit {
  slug: any;
  data: any;
  careerData: any;
  jobData: any;
  constructor(private apiservice: ApiService, private title: Title, private meta: Meta) {

  }

  ngOnInit(): void {
    this.getCareerContent();
    this.getJobs();
  }
  getCareerContent() {
    this.apiservice.getCareer().subscribe((res) => {
      this.data = <any>res;
      this.careerData = this.data;
      this.title.setTitle(this.careerData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.careerData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.careerData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.careerData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.careerData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/career/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });

    },
      (error) => {
        console.log(error)
      })
  }

  getJobs() {
    this.apiservice.getJobs().subscribe((res) => {
      this.data = <any>res;
      this.jobData = this.data;
      console.log(this.jobData)

    },
      (error) => {
        console.log(error)
      })
  }


}
