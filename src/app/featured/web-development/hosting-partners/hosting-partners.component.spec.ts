import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HostingPartnersComponent } from './hosting-partners.component';

describe('HostingPartnersComponent', () => {
  let component: HostingPartnersComponent;
  let fixture: ComponentFixture<HostingPartnersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HostingPartnersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HostingPartnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
