import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-hosting-partners',
  templateUrl: './hosting-partners.component.html',
  styleUrls: ['./hosting-partners.component.css']
})
export class HostingPartnersComponent implements OnInit {

  content: any;
  constructor(private title: Title, private meta: Meta) { }
  ngOnInit() {
    this.title.setTitle('Our Hosting Partners');
    this.meta.addTag({ name: "description", content: "Hosting Partners,AHA Technocrats" });
    this.meta.addTag({ name: "keywords", content: "Hosting Partners,AHA Technocrats" });
    this.meta.addTag({ property: "og:locale", content: "en_US" });
    this.meta.addTag({ property: "og:type", content: "article" });
    this.meta.addTag({ property: "og:title", content: 'Our Hosting Partners' });
    this.meta.addTag({ property: "og:description", content: "Hosting Partners -AHA Technocrats" });
    this.meta.addTag({ property: "og:url", content: "http://www.ahatechnocrats.com/web-development/hosting-partners" });
    this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
    this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
    this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
  }


}
