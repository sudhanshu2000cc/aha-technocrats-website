import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonwithslugComponent } from './commonwithslug/commonwithslug.component';
import { Route, RouterModule } from '@angular/router';
import { ContactModule } from '../../contact/contact.module';



const routes: Route[] = [

  { path: ':slug', component: CommonwithslugComponent },


];

@NgModule({
  declarations: [CommonwithslugComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), ContactModule
  ]
})
export class WebDesigningModule { }
