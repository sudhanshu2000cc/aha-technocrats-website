import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonwithslugComponent } from './commonwithslug.component';

describe('CommonwithslugComponent', () => {
  let component: CommonwithslugComponent;
  let fixture: ComponentFixture<CommonwithslugComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommonwithslugComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonwithslugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
