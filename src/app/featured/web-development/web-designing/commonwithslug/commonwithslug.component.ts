import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../shared/api.service';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-commonwithslug',
  templateUrl: './commonwithslug.component.html',
  styleUrls: ['./commonwithslug.component.css']
})
export class CommonwithslugComponent implements OnInit {
  commonData: any;
  data: any;
  slug: any;
  constructor(private apiservice: ApiService, private route: ActivatedRoute, private title: Title, private meta: Meta) {

    this.route.params.subscribe((params) => {
      this.slug = params.slug;
      console.log(this.slug);
      if (this.slug == "bootstrap-developer") {
        this.getCommonData('87');
      } else if (this.slug == "html-5") {
        this.getCommonData('88');
      } else if (this.slug == "responsive-website-designer") {
        this.getCommonData('89');
      } else if (this.slug == "designshop") {
        this.getCommonData('90');
      } else if (this.slug == "photoshop") {
        this.getCommonData('91');
      } else if (this.slug == "corel-draw") {
        this.getCommonData('92');
      } else if (this.slug == "web2.0") {
        this.getCommonData('93');
      } else if (this.slug == "mobile-apps-design") {
        this.getCommonData('101');
      }

    });
  }

  ngOnInit(): void {
  }

  getCommonData(postId: any) {
    this.apiservice.getCommonDataforWebDevelopment(postId).subscribe((res) => {
      this.data = <any>res;
      this.commonData = this.data;
      this.title.setTitle(this.commonData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.commonData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.commonData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "http://www.ahatechnocrats.com/web-development/php-development/laravel-developer/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
    },
      (error) => {
        console.log(error)
      })
  }
}
