import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../shared/api.service';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-phpdevelopment',
  templateUrl: './phpdevelopment.component.html',
  styleUrls: ['./phpdevelopment.component.css']
})
export class PhpdevelopmentComponent implements OnInit {
  commonData: any;
  data: any;
  slug: any;
  constructor(private apiservice: ApiService, private route: ActivatedRoute, private title: Title, private meta: Meta) {

    this.getCommonData('79');

  }

  ngOnInit(): void {
  }
  getCommonData(postId: any) {
    this.apiservice.getCommonDataforWebDevelopment(postId).subscribe((res) => {
      this.data = <any>res;
      this.commonData = this.data;
      this.title.setTitle(this.commonData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.commonData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.commonData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "http://www.ahatechnocrats.com/web-development/php-development/laravel-developer/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });

    },
      (error) => {
        console.log(error)
      })
  }
}
