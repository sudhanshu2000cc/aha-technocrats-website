import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, Routes, Route } from '@angular/router';
import { PhpdevelopmentComponent } from './phpdevelopment.component';
import { CommonWithSlugComponent } from './common-with-slug/common-with-slug.component';
import { ContactModule } from '../../contact/contact.module';



const routes: Route[] = [

  { path: ':slug', component: CommonWithSlugComponent },
  { path: '', component: PhpdevelopmentComponent },

];

@NgModule({
  declarations: [PhpdevelopmentComponent, CommonWithSlugComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), ContactModule
  ]
})
export class PhpDevelopmentModule { }
