import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhpdevelopmentComponent } from './phpdevelopment.component';

describe('PhpdevelopmentComponent', () => {
  let component: PhpdevelopmentComponent;
  let fixture: ComponentFixture<PhpdevelopmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhpdevelopmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhpdevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
