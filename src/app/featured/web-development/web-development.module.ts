import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, Routes, Route } from '@angular/router';
import { HostingPartnersComponent } from './hosting-partners/hosting-partners.component';



const routes: Route[] =
  [

    { path: 'php-development', loadChildren: () => import('./php-development/php-development.module').then(m => m.PhpDevelopmentModule) },
    { path: 'web-designing', loadChildren: () => import('./web-designing/web-designing.module').then(m => m.WebDesigningModule) },
    { path: 'hosting-partners', component: HostingPartnersComponent },

  ];
@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class WebDevelopmentModule { }
