import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Meta, Title } from '@angular/platform-browser';
@Component({
  selector: 'app-outsourcing',
  templateUrl: './outsourcing.component.html',
  styleUrls: ['./outsourcing.component.css']
})
export class OutsourcingComponent implements OnInit {
  data: any;
  outsourcingData: any;
  constructor(private apiservice: ApiService, private title: Title, private meta: Meta) { }

  ngOnInit(): void {
    this.getOutSourcingData();
  }

  getOutSourcingData() {
    this.apiservice.getOutsourcing().subscribe((res) => {
      this.data = <any>res;
      this.outsourcingData = this.data;
      this.title.setTitle(this.outsourcingData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.outsourcingData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.outsourcingData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.outsourcingData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.outsourcingData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/portfolio/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });

    },
      (error) => {
        console.log(error)
      })
  }
}
