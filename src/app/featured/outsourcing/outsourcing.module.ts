import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutsourcingComponent } from './outsourcing.component';
import { Route, RouterModule } from '@angular/router';
const routes: Route[] =
  [{ path: 'it-outsourcing', component: OutsourcingComponent }];

@NgModule({
  declarations: [OutsourcingComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes),
  ]
})
export class OutsourcingModule { }
