import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  data: any;
  submitted: boolean = false;
  contactForm = this.fb.group({
    name: [''],
    email: [''],
    message: [''],
    phoneNo: [''],
    services: [''],
    skype: ['']

  })
  constructor(private fb: FormBuilder, private apiService: ApiService) { }

  ngOnInit(): void {

  }

  get f() { return this.contactForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.contactForm.invalid) {
      this.submitted = false;
      return;
    }

    if (this.contactForm.valid) {

      this.apiService.contact(this.contactForm.value).subscribe(
        (res) => {
          this.data = <any>res;
          // document.getElementById('exampleModalCenterSuccess').classList.add('show')
          console.log(this.data);
          this.contactForm.reset();
        }, (error) => {
          // document.getElementById('exampleModalCenterUnsuccess').classList.add('show')
          this.contactForm.reset();
          console.log(error)
        })
    }
  }

}
