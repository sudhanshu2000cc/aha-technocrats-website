import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-lets-connect',
  templateUrl: './lets-connect.component.html',
  styleUrls: ['./lets-connect.component.css']
})
export class LetsConnectComponent implements OnInit {
  data: any;
  submitted: boolean = false;
  letsConnect = this.fb.group({
    name: [''],
    email: [''],
    skype: [''],
    message: ['']
  })
  constructor(private fb: FormBuilder, private apiService: ApiService) { }

  ngOnInit(): void {
  }
  get f() { return this.letsConnect.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.letsConnect.invalid) {
      this.submitted = false;
      return;
    }

    if (this.letsConnect.valid) {

      this.apiService.contactUs(this.letsConnect.value).subscribe(
        (res) => {
          this.data = <any>res;
          // document.getElementById('exampleModalCenterSuccess').classList.add('show')
          console.log(this.data);
          this.letsConnect.reset();
        }, (error) => {
          // document.getElementById('exampleModalCenterUnsuccess').classList.add('show')
          this.letsConnect.reset();
          console.log(error)
        })
    }
  }

}
