import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/shared/api.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  submitted: boolean = false;
  data: any;
  contactUsForm = this.fb.group({
    name: [''],
    email: [''],
    phone: [''],
    website: [''],
    instant_messenger: [''],
    instant_messenger_details: [''],
    budget: [''],
    projectdate: [''],
    message: [''],
    comments: [''],
  })
  constructor(private fb: FormBuilder, private apiService: ApiService) { }

  ngOnInit(): void {
  }

  get f() { return this.contactUsForm.controls; }
  onSubmit() {
    this.submitted = true;
    if (this.contactUsForm.invalid) {
      this.submitted = false;
      return;
    }

    if (this.contactUsForm.valid) {

      this.apiService.contactUs(this.contactUsForm.value).subscribe(
        (res) => {
          this.data = <any>res;
          // document.getElementById('exampleModalCenterSuccess').classList.add('show')
          console.log(this.data);
          this.contactUsForm.reset();
        }, (error) => {
          // document.getElementById('exampleModalCenterUnsuccess').classList.add('show')
          this.contactUsForm.reset();
          console.log(error)
        })
    }
  }
}
