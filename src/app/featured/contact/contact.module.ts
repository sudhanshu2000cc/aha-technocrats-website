import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact/contact.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LetsConnectComponent } from './lets-connect/lets-connect.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ContactComponent, ContactUsComponent, LetsConnectComponent,],
  imports: [
    CommonModule, ReactiveFormsModule
  ],
  exports: [ContactComponent, ContactUsComponent, LetsConnectComponent]
})
export class ContactModule { }
