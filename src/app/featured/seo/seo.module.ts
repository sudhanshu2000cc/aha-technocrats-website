import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonWithSlugComponent } from './common-with-slug/common-with-slug.component';
import { RouterModule, Route } from '@angular/router';
import { ContactModule } from '../contact/contact.module';
const routes: Route[] = [
  { path: ':slug', component: CommonWithSlugComponent }
];

@NgModule({
  declarations: [CommonWithSlugComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), ContactModule
  ]
})
export class SeoModule { }
