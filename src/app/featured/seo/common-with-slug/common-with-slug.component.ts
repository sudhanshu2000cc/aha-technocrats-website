import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../shared/api.service';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-common-with-slug',
  templateUrl: './common-with-slug.component.html',
  styleUrls: ['./common-with-slug.component.css']
})
export class CommonWithSlugComponent implements OnInit {
  commonData: any;
  data: any;
  slug: any;
  constructor(private apiservice: ApiService, private route: ActivatedRoute, private title: Title, private meta: Meta) {

    this.route.params.subscribe((params) => {
      this.slug = params.slug;
      if (this.slug == "on-page-optimization") {
        this.getCommonData('210');
      } else if (this.slug == "off-page-optimization") {
        this.getCommonData('211');
      } else if (this.slug == "link-building") {
        this.getCommonData('221');
      } else if (this.slug == "local-listing") {
        this.getCommonData('222');

      } else if (this.slug == "google-analytics-and-webmaster") {
        this.getCommonData('223');

      } else if (this.slug == "panda-penguin-recovery") {
        this.getCommonData('224');

      } else if (this.slug == "pay-per-click") {
        this.getCommonData('214');

      } else if (this.slug == "social-media-optimization") {
        this.getCommonData('225');

      } else if (this.slug == "email-marketing") {
        this.getCommonData('226');

      } else if (this.slug == "google-adsence") {
        this.getCommonData('227');

      } else if (this.slug == "content-writing") {
        this.getCommonData('213');

      } else if (this.slug == "seo-packages") {
        this.getCommonData('212');

      } else if (this.slug == "why-us") {
        this.getCommonData('228');

      } else if (this.slug == "how-we-do") {
        this.getCommonData('229');
      }

    });
  }

  ngOnInit(): void {
  }
  getCommonData(postId: any) {
    this.apiservice.getCommonDataforWebDevelopment(postId).subscribe((res) => {
      this.data = <any>res;
      this.commonData = this.data;
      this.title.setTitle(this.commonData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.commonData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.commonData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "http://www.ahatechnocrats.com/web-development/php-development/laravel-developer/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
    },
      (error) => {
        console.log(error)
      })
  }
}
