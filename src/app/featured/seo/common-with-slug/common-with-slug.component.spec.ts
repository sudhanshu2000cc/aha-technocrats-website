import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonWithSlugComponent } from './common-with-slug.component';

describe('CommonWithSlugComponent', () => {
  let component: CommonWithSlugComponent;
  let fixture: ComponentFixture<CommonWithSlugComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommonWithSlugComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonWithSlugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
