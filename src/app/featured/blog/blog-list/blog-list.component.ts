import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/api.service';
import { Meta, Title } from '@angular/platform-browser';
@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {
  blogListData: any;
  data: any;
  p: number = 1;
  constructor(private apiservice: ApiService, private title: Title, private meta: Meta) { }

  ngOnInit(): void {
    this.getBlogList();
  }
  getBlogList() {
    this.apiservice.getBlogList().subscribe((res) => {
      this.data = <any>res;
      this.blogListData = this.data;
      this.title.setTitle("Our Blogs");

      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: "Our Blogs" });
      this.meta.addTag({ property: "og:description", content: "Our Company Vision We will increase the value of our company and our global portfolio of diversified brands by exceeding customers&#8217; expectations and achieving market leadership and operating excellence in every segment of our company. Since 2007 Ut sit amet dignissim libero. Integer scelerisque, eros interdum suscipit rhoncus, mauris felis eleifend libero, a adipiscing arcu &hellip; Continue reading &quot;About Us&quot;" });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/about-us/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });

    },
      (error) => {
        console.log(error)
      })
  }
}
