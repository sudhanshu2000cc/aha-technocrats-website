import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../shared/api.service';
import { Meta, Title } from '@angular/platform-browser';
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  blogDetailData: any;
  data: any;
  slug: any;
  tagsData: any;
  constructor(private apiservice: ApiService, private route: ActivatedRoute, private title: Title, private meta: Meta) {

    this.route.params.subscribe((params) => {
      this.slug = params.slug;
      this.getBlogDetail();
      this.getagsid();
    });

  }

  ngOnInit(): void {
  }
  getBlogDetail() {
    this.apiservice.getPostid(this.slug).subscribe((res) => {
      this.data = <any>res;
      this.blogDetailData = this.data[0];

      this.title.setTitle(this.blogDetailData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.blogDetailData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.blogDetailData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.blogDetailData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: "Our Company Vision We will increase the value of our company and our global portfolio of diversified brands by exceeding customers&#8217; expectations and achieving market leadership and operating excellence in every segment of our company. Since 2007 Ut sit amet dignissim libero. Integer scelerisque, eros interdum suscipit rhoncus, mauris felis eleifend libero, a adipiscing arcu &hellip; Continue reading &quot;About Us&quot;" });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/about-us/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
    },
      (error) => {
        console.log(error)
      })
  }

  getagsid() {

    this.apiservice.getagsid(this.blogDetailData.tags).subscribe((res) => {
      this.data = <any>res;
      this.tagsData = this.data;
      console.log(this.tagsData);
      this.title.setTitle(this.blogDetailData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.blogDetailData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.blogDetailData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.blogDetailData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: "Our Company Vision We will increase the value of our company and our global portfolio of diversified brands by exceeding customers&#8217; expectations and achieving market leadership and operating excellence in every segment of our company. Since 2007 Ut sit amet dignissim libero. Integer scelerisque, eros interdum suscipit rhoncus, mauris felis eleifend libero, a adipiscing arcu &hellip; Continue reading &quot;About Us&quot;" });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/about-us/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
    },
      (error) => {
        console.log(error)
      })

  }
}
