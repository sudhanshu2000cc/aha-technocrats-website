import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  data: any;
  resentblogData: any;
  constructor(private apiservice: ApiService) {
    this.getHomeBlogData();
  }

  ngOnInit(): void {
  }
  getHomeBlogData() {
    this.apiservice.getHomeBlogData().subscribe((res) => {
      this.data = <any>res;
      this.resentblogData = this.data;

    },
      (error) => {
        console.log(error)
      })
  }
}
