import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogComponent } from './blog.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
export const routeblog = [
  {
    path: "",
    component: BlogComponent,
    children: [
      {
        path: ":slug",
        component: BlogDetailComponent,
        pathMatch: "full",
      },
      {
        path: ":year/:month/:day/:slug",
        component: BlogDetailComponent,
        pathMatch: "full",
      },
      { path: ":year/:month", component: BlogListComponent, pathMatch: "full" },
      { path: "", component: BlogListComponent, pathMatch: "full" },
      { path: "**", redirectTo: "/404" },
    ],
  },
];
@NgModule({
  declarations: [BlogComponent, BlogListComponent, BlogDetailComponent],
  imports: [
    CommonModule, RouterModule.forChild(routeblog), NgxPaginationModule
  ]
})
export class BlogModule { }
