import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonWithSlugComponent } from './common-with-slug/common-with-slug.component';
import { Route, RouterModule } from '@angular/router';
import { MobileApplicationComponent } from '../mobile-application/mobile-application.component';
import { ContactModule } from '../contact/contact.module';

const routes: Route[] = [
  { path: ':slug', component: CommonWithSlugComponent },
  { path: '', component: MobileApplicationComponent },


];

@NgModule({
  declarations: [CommonWithSlugComponent, MobileApplicationComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), ContactModule
  ]
})
export class MobileApplicationModule { }
