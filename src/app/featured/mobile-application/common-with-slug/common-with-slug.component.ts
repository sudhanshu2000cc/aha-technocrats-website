import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/shared/api.service';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-common-with-slug',
  templateUrl: './common-with-slug.component.html',
  styleUrls: ['./common-with-slug.component.css']
})
export class CommonWithSlugComponent implements OnInit {

  commonData: any;
  data: any;
  slug: any;
  constructor(private apiservice: ApiService, private route: ActivatedRoute, private title: Title, private meta: Meta) {

    this.route.params.subscribe((params) => {
      this.slug = params.slug;

      if (this.slug == "android-development") {
        this.getCommonData('177');
      } else if (this.slug == "iphone-development") {
        this.getCommonData('178');
      } else if (this.slug == "mobile-applications") {
        this.getCommonData('38');
      }

    });
  }

  ngOnInit(): void {
  }
  getCommonData(postId: any) {
    this.apiservice.getCommonDataforWebDevelopment(postId).subscribe((res) => {
      this.data = <any>res;
      this.commonData = this.data;
   
      this.title.setTitle(this.commonData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.commonData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.commonData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.commonData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "http://www.ahatechnocrats.com/web-development/php-development/laravel-developer/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
    },
      (error) => {
        console.log(error)
      })
  }
}
