import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Meta, Title } from '@angular/platform-browser';
@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  data: any;
  portfolioData: any;
  constructor(private apiservice: ApiService, private title: Title, private meta: Meta) { }

  ngOnInit(): void {
    this.getportfolioData();
  }
  getportfolioData() {
    this.apiservice.getPortfolio().subscribe((res) => {
      this.data = <any>res;
      this.portfolioData = this.data;
      this.title.setTitle(this.portfolioData['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.portfolioData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.portfolioData['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.portfolioData['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.portfolioData['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/portfolio/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });

    },
      (error) => {
        console.log(error)
      })
  }
}
