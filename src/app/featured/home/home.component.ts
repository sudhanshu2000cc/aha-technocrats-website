import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Meta, Title } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private title: Title, private meta: Meta, private apiservice: ApiService) { }
  featuredContentData: any;
  data: any;
  aboutContentData: any;
  serviceContentData: any;
  ourProjectContentData: any;
  testimonialData: any;
  blogData: any;
  partnerLogo: any;
  home: any;
  ngOnInit(): void {



    this.apiservice.getHome().subscribe((res) => {
      this.home = <any>res;
      this.title.setTitle(this.home['yoast_meta'].yoast_wpseo_title);
      this.meta.addTag({ name: "description", content: this.home['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ name: "keywords", content: this.home['yoast_meta'].yoast_wpseo_metakeywords });
      this.meta.addTag({ property: "og:locale", content: "en_US" });
      this.meta.addTag({ property: "og:type", content: "article" });
      this.meta.addTag({ property: "og:title", content: this.home['yoast_meta'].yoast_wpseo_title });
      this.meta.addTag({ property: "og:description", content: this.home['yoast_meta'].yoast_wpseo_metadesc });
      this.meta.addTag({ property: "og:url", content: "https://www.ahatechnocrats.com/" });
      this.meta.addTag({ property: "og:site_name", content: "AHA Technocrats" });
      this.meta.addTag({ property: "article:publisher", content: "https://www.facebook.com/ahatechnocrats" });
      this.meta.addTag({ property: "article:author", content: "https://www.facebook.com/aha.technocrats" });
      this.meta.addTag({ property: "og:image", content: "https://www.ahatechnocrats.com/assets/og-image/home.png" });
    })
    this.getFeaturesContent();
    this.getAboutContent();
    this.getServiceContent();
    this.getOurProjectContent();
    this.getTestimonialData();
    this.getHomeBlogData();
    this.getPartnerLogo();
  }

  getFeaturesContent() {
    this.apiservice.getFeaturesContent().subscribe((res) => {
      this.data = <any>res;
      this.featuredContentData = this.data;
    },
      (error) => {
        console.log(error)
      })
  }

  getAboutContent() {
    this.apiservice.getAboutContent().subscribe((res) => {
      this.data = <any>res;
      this.aboutContentData = this.data;
      setTimeout(function () {
        $(".counter").counterUp({
          delay: 10,
          time: 1000,
        });
      }, 0);
    },
      (error) => {
        console.log(error)
      })
  }

  getServiceContent() {
    this.apiservice.getServiceContent().subscribe((res) => {
      this.data = <any>res;
      this.serviceContentData = this.data;

    },
      (error) => {
        console.log(error)
      })
  }

  getOurProjectContent() {
    this.apiservice.getOurProjectContent().subscribe((res) => {
      this.data = <any>res;
      this.ourProjectContentData = this.data;
    },
      (error) => {
        console.log(error)
      })
  }

  getTestimonialData() {
    this.apiservice.getTestimonialData().subscribe((res) => {
      this.data = <any>res;
      this.testimonialData = this.data;
      setTimeout(function () {
        $("#customers-testimonials").owlCarousel({
          loop: true,
          center: true,
          items: 3,
          margin: 0,
          autoplay: false,
          dots: true,
          autoplayTimeout: 8000,
          smartSpeed: 450,
          responsive: {
            0: {
              items: 1,
            },
            768: {
              items: 2,
            },
            1170: {
              items: 2,
            },
          },
        });

      }, 0);
    },
      (error) => {
        console.log(error)
      })
  }
  getHomeBlogData() {
    this.apiservice.getHomeBlogData().subscribe((res) => {
      this.data = <any>res;
      this.blogData = this.data;

    },
      (error) => {
        console.log(error)
      })
  }

  getPartnerLogo() {
    this.apiservice.getPartnerLogo().subscribe((res) => {
      this.data = <any>res;
      this.partnerLogo = this.data;
      setTimeout(function () {
        $(".owl-carousel").owlCarousel({
          loop: true,
          autoplay: true,
          margin: 10,
          responsiveClass: true,
          autoplaySpeed: 100,
          smartSpeed: 2000,
          nav: false,
          dots: false,
          responsive: {
            0: {
              items: 2,
            },
            576: {
              items: 3,
            },
            768: {
              items: 4,
            },
            1200: {
              items: 5,
            },
          }

        });

      }, 0);

    },
      (error) => {
        console.log(error)
      })
  }

  revealVideo() {

  }
}
