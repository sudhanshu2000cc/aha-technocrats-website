import { Injectable } from "@angular/core";
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import { SpinnerService } from "../common/spinner/spinner.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private spinnerService: SpinnerService) { }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        this.spinnerService.requestStarted();
        return next.handle(request)
            .pipe(
                tap((event: any) => {
                    if (event instanceof HttpResponse) {
                        this.spinnerService.requestEnded();
                    }
                },

                    (error: HttpErrorResponse) => {
                        this.spinnerService.resetSpinner();

                        throw error;
                    }
                ))
    }
}