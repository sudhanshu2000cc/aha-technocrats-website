import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseApiUrl = "http://newapi.ahadev.net/wp-json/wp/v2";
  constructor(private http: HttpClient) { }


  getHome() {
    return this.http.get(this.baseApiUrl + "/pages/393").pipe(catchError(this.handleError));;
  }
  getClients() {
    return this.http.get(this.baseApiUrl + "/our_clients?_embed=true&per_page=30").pipe(catchError(this.handleError));;
  }
  // for home page latest blog api
  getPost() {
    return this.http.get(this.baseApiUrl + "/posts?_embed=true&per_page=1").pipe(catchError(this.handleError));;
  }
  // for blog page api with pagination
  getBlogList() {
    return this.http.get(this.baseApiUrl + "/posts?").pipe(catchError(this.handleError));;
  }
  getPostid(slug1: any) {
    return this.http.get(this.baseApiUrl + '/posts?_embed=true&slug=' + slug1).pipe(catchError(this.handleError));;
  }

  getagsid(slug1: any) {
    return this.http.get(this.baseApiUrl + '/tags/' + slug1).pipe(catchError(this.handleError));;
  }

  getComment(postId: any) {
    return this.http.get(this.baseApiUrl + '/comments?_embed&post=' + postId).pipe(catchError(this.handleError));;
  }
  postComment(data: Comment) {
    return this.http.post(this.baseApiUrl + '/comments?', data).pipe(catchError(this.handleError));;
  }

  contact(data: Contact) {
    return this.http.post<any>(this.baseApiUrl + '/contact_us_mail', data).pipe(catchError(this.handleError));;
  }

  contactUs(data: Contact) {
    return this.http.post<any>(this.baseApiUrl + '/web-deplopment-contact', data).pipe(catchError(this.handleError));;
  }

  getMaintenance() {
    return this.http.get(this.baseApiUrl + "/pages/2033").pipe(catchError(this.handleError));;
  }

  getPosttags(slug: any) {
    return this.http.get('https://api.ahatechnocrats.com/wp-json/tag/' + slug).pipe(catchError(this.handleError));;
  }

  getTestimonial() {
    return this.http.get(this.baseApiUrl + "/pages/363").pipe(catchError(this.handleError));;
  }

  getCareer() {
    return this.http.get(this.baseApiUrl + "/pages/4").pipe(catchError(this.handleError));;
  }

  getJobs() {
    return this.http.get(this.baseApiUrl + "/career_job").pipe(catchError(this.handleError));;
  }


  getAbout() {
    return this.http.get(this.baseApiUrl + "/pages/32").pipe(catchError(this.handleError));;
  }

  getPortfolio() {
    return this.http.get(this.baseApiUrl + "/pages/43").pipe(catchError(this.handleError));;
  }

  // getAndroidApplication() {
  //   return this.http.get(this.baseApiUrl + "/pages/190");
  // }

  // getAndroidClientServerApplication() {
  //   return this.http.get(this.baseApiUrl + "/pages/183");
  // }

  // getAndroidGame() {
  //   return this.http.get(this.baseApiUrl + "/pages/191");
  // }
  // getIphoneApplication() {
  //   return this.http.get(this.baseApiUrl + "/pages/195");
  // }
  // getIphoneClientServerApplication() {
  //   return this.http.get(this.baseApiUrl + "/pages/197");
  // }

  // getIphoneGame() {
  //   return this.http.get(this.baseApiUrl + "/pages/196");
  // }
  // getMobileApplication() {
  //   return this.http.get(this.baseApiUrl + "/pages/38");
  // }
  // getAndroidDevelopment() {
  //   return this.http.get(this.baseApiUrl + "/pages/177");
  // }
  // getIphoneDevelopment() {
  //   return this.http.get(this.baseApiUrl + "/pages/178");
  // }

  //web Development page api


  getCommonDataforWebDevelopment(postId: any) {
    return this.http.get(this.baseApiUrl + "/pages/" + postId).pipe(catchError(this.handleError));;
  }
  // getWebDev() {
  //   return this.http.get(this.baseApiUrl + "/pages/35");
  // }
  // getPhpDev() {
  //   return this.http.get(this.baseApiUrl + "/pages/79");
  // }
  // getCakephp() {
  //   return this.http.get(this.baseApiUrl + "/pages/62");
  // }
  // getCodeigniter() {
  //   return this.http.get(this.baseApiUrl + "/pages/58");
  // }
  // getCustomizedphp() {
  //   return this.http.get(this.baseApiUrl + "/pages/59");
  // }
  // getDrupal() {
  //   return this.http.get(this.baseApiUrl + "/pages/61");
  // }
  // getLaravel() {
  //   return this.http.get(this.baseApiUrl + "/pages/2013");
  // }
  // getMagento() {
  //   return this.http.get(this.baseApiUrl + "/pages/68");
  // }
  // getMobileWeb() {
  //   return this.http.get(this.baseApiUrl + "/pages/71");
  // }
  // getOpencart() {
  //   return this.http.get(this.baseApiUrl + "/pages/69");
  // }
  // getSmarty() {
  //   return this.http.get(this.baseApiUrl + "/pages/70");
  // }
  // getVuejs() {
  //   return this.http.get(this.baseApiUrl + "/pages/2549");
  // }
  // getWordpress() {
  //   return this.http.get(this.baseApiUrl + "/pages/60");
  // }
  // getyii() {
  //   return this.http.get(this.baseApiUrl + "/pages/1421");
  // }
  // getZend() {
  //   return this.http.get(this.baseApiUrl + "/pages/1367");
  // }

  //web designing page api

  // getWebdesign() {
  //   return this.http.get(this.baseApiUrl + "/pages/80");
  // }
  // getBootstrap() {
  //   return this.http.get(this.baseApiUrl + "/pages/87");
  // }
  // getCoralDrow() {
  //   return this.http.get(this.baseApiUrl + "/pages/92");
  // }
  // getDesignShop() {
  //   return this.http.get(this.baseApiUrl + "/pages/90");
  // }
  // getHtml() {
  //   return this.http.get(this.baseApiUrl + "/pages/88");
  // }
  // getMobileApps() {
  //   return this.http.get(this.baseApiUrl + "/pages/101");
  // }
  // getPhotoshop() {
  //   return this.http.get(this.baseApiUrl + "/pages/91");
  // }
  // getResponsiveWeb() {
  //   return this.http.get(this.baseApiUrl + "/pages/89");
  // }
  // getWeb() {
  //   return this.http.get(this.baseApiUrl + "/pages/93");
  // }

  // Outsourcing api 
  getOutsourcing() {
    return this.http.get(this.baseApiUrl + "/pages/934");
  }
  // getContentWriting() {
  //   return this.http.get(this.baseApiUrl + "/pages/213");
  // }

  // getEmailMarketing() {
  //   return this.http.get(this.baseApiUrl + "/pages/226");
  // }

  // getGoogleAdsence() {
  //   return this.http.get(this.baseApiUrl + "/pages/227");
  // }
  // GoogleAnalyticsAndWebmaster() {
  //   return this.http.get(this.baseApiUrl + "/pages/223");
  // }
  // getHowWeDo() {
  //   return this.http.get(this.baseApiUrl + "/pages/229");
  // }
  // getLinkBuilding() {
  //   return this.http.get(this.baseApiUrl + "/pages/221");
  // }
  // getLocalListing() {
  //   return this.http.get(this.baseApiUrl + "/pages/222");
  // }
  // getOffPageOptimization() {
  //   return this.http.get(this.baseApiUrl + "/pages/211");
  // }
  // getOnPageOptimisation() {
  //   return this.http.get(this.baseApiUrl + "/pages/210");
  // }
  // getPandaPenguinRecovery() {
  //   return this.http.get(this.baseApiUrl + "/pages/224");
  // }
  // getPayPerClick() {
  //   return this.http.get(this.baseApiUrl + "/pages/214");
  // }
  // getSeoPackages() {
  //   return this.http.get(this.baseApiUrl + "/pages/212");
  // }
  // getSeoRequest() {
  //   return this.http.get(this.baseApiUrl + "/pages/974");
  // }
  // getSocialMediaOptimizations() {
  //   return this.http.get(this.baseApiUrl + "/pages/225");
  // }
  // getWhyUs() {
  //   return this.http.get(this.baseApiUrl + "/pages/228");
  // }
  // getSeo() {
  //   return this.http.get(this.baseApiUrl + "/pages/41");
  // }

  // getContactUs() {
  //   return this.http.get(this.baseApiUrl + "/pages/22");
  // }
  getPrivacyPolicy() {
    return this.http.get(this.baseApiUrl + "/pages/2514");
  }
  getSitemap() {
    return this.http.get(this.baseApiUrl + "/pages/730");
  }
  getTermsConditions() {
    return this.http.get(this.baseApiUrl + "/pages/2521");
  }
  getFaq() {
    return this.http.get(this.baseApiUrl + "/pages/13");
  }

  getFeaturesContent() {
    return this.http.get(this.baseApiUrl + "/pages/3370").pipe(catchError(this.handleError));
  }
  getAboutContent() {
    return this.http.get(this.baseApiUrl + "/pages/3500").pipe(catchError(this.handleError));
  }
  getServiceContent() {
    return this.http.get(this.baseApiUrl + "/pages/3507").pipe(catchError(this.handleError));
  }
  getOurProjectContent() {
    return this.http.get(this.baseApiUrl + "/pages/3511").pipe(catchError(this.handleError));
  }

  getTestimonialData() {
    return this.http.get(this.baseApiUrl + "/testimonial").pipe(catchError(this.handleError));
  }
  getHomeBlogData() {
    return this.http.get(this.baseApiUrl + "/posts?per_page=3&page=1").pipe(catchError(this.handleError));
  }
  getPartnerLogo() {
    return this.http.get(this.baseApiUrl + "/our_clients").pipe(catchError(this.handleError));
  }
  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

export interface Contact {
  name: string;
  email: string;
  message: string;
}
export interface Comment {
  post: number,
  author_name: string,
  author_url: string,
  author_email: string,
  content: string
}
export interface Replycom {
  post: number,
  parent: number,
  author_name: string,
  author_url: string,
  author_email: string,
  content: string
}