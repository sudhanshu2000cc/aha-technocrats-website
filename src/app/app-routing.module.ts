import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonComponent } from './common/common.component';
import { PagenotfoundComponent } from './common/pagenotfound/pagenotfound.component';
import { AboutUsComponent } from './featured/aboutUs/about-us/about-us.component';
import { CareerComponent } from './featured/aboutUs/career/career.component';
import { TestimonialsComponent } from './featured/aboutUs/testimonials/testimonials.component';
import { ContactComponent } from './featured/contact/contact/contact.component';
import { HomeComponent } from './featured/home/home.component';
import { PortfolioComponent } from './featured/portfolio/portfolio.component';

const routes: Routes = [

  { path: "404", component: PagenotfoundComponent },
  {
    path: "", component: CommonComponent,
    children: [
      { path: "about-us", component: AboutUsComponent },
      { path: "career", component: CareerComponent },
      { path: "testimonials", component: TestimonialsComponent },
      { path: 'web-development', loadChildren: () => import('./featured/web-development/web-development.module').then(m => m.WebDevelopmentModule) },
      { path: "blog", loadChildren: () => import('./featured/blog/blog.module').then(m => m.BlogModule) },
      { path: "mobile-applications", loadChildren: () => import('./featured/mobile-application/mobile-application.module').then(m => m.MobileApplicationModule) },
      { path: "seo-services", loadChildren: () => import('./featured/seo/seo.module').then(m => m.SeoModule) },
      { path: "outsourcing", loadChildren: () => import('./featured/outsourcing/outsourcing.module').then(m => m.OutsourcingModule) },
      { path: "contact-us", component: ContactComponent },
      { path: "portfolio", component: PortfolioComponent },
      { path: "", component: HomeComponent },
      { path: "**", redirectTo: "/404" },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
